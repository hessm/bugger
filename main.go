package main

import (
	"gitlab.eclipse.org/eclipse/xfsc/libraries/messaging/cloudeventprovider"
	"log"
	"sync"
	"time"
)

func publishExceptionMessage(c *cloudeventprovider.CloudEventProviderClient, tickDuration time.Duration, wg *sync.WaitGroup) {
	for _ = range time.Tick(tickDuration) {
		now := time.Now()
		data := map[string]any{"exception": true, "error": "NullPointerException", "time": now}

		event, err := cloudeventprovider.NewEvent("bugger/exception", "exception.v1", data)
		if err != nil {
			log.Fatal(err)
		}

		err = c.Pub(event)
		if err != nil {
			log.Fatalf("failed to send, %v", err)
		} else {
			log.Printf("published exception event")
		}
	}
	wg.Done()
}

func publishSuccessMessage(c *cloudeventprovider.CloudEventProviderClient, tickDuration time.Duration, wg *sync.WaitGroup) {
	for _ = range time.Tick(tickDuration) {
		now := time.Now()
		data := map[string]any{"success": true, "message": "permission granted", "time": now}

		event, err := cloudeventprovider.NewEvent("bugger/success", "success.v1", data)
		if err != nil {
			log.Fatal(err)
		}

		if err := c.Pub(event); err != nil {
			log.Fatalf("failed to send, %v", err)
		} else {
			log.Printf("published success event")
		}
	}
	wg.Done()
}

func publishOfferMessage(c *cloudeventprovider.CloudEventProviderClient, tickDuration time.Duration, wg *sync.WaitGroup) {
	for _ = range time.Tick(tickDuration) {
		now := time.Now()
		data := map[string]any{"twoFactor": map[string]any{"enabled": true, "recipientType": "email", "recipientAddress": "test@gmail.com"}, "ttl": 10 * time.Hour, "time": now}

		event, err := cloudeventprovider.NewEvent("bugger/offer", "offer.v1", data)
		if err != nil {
			log.Fatal(err)
		}

		reply, err := c.Request(event, 30*time.Second)
		if err != nil {
			log.Fatalf("failed to send, %v", err)
		} else {
			log.Printf("published offer event and got reply: %v", reply)
		}
	}
	wg.Done()
}

func main() {
	cReq, err := cloudeventprovider.NewClient(cloudeventprovider.Req, "offers")
	if err != nil {
		log.Fatal(err)
	}
	defer cReq.Close()
	cPub, err := cloudeventprovider.NewClient(cloudeventprovider.Pub, "events")
	if err != nil {
		log.Fatal(err)
	}
	defer cPub.Close()

	var wg sync.WaitGroup

	wg.Add(1)
	go publishOfferMessage(cReq, 5*time.Second, &wg)
	//go publishExceptionMessage(cPub, 10*time.Second, &wg)
	//go publishSuccessMessage(cPub, 5*time.Second, &wg)

	wg.Wait()
}
